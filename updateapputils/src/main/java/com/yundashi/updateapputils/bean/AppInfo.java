package com.yundashi.updateapputils.bean;

public class AppInfo {
    public int uid;
    public String label;//应用名称
    public String package_name;//应用包名

    public AppInfo() {
        uid = 0;
        label = "";
        package_name = "";
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }
}
