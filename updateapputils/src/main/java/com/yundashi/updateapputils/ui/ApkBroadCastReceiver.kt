package com.yundashi.updateapputils.ui

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Handler
import com.yundashi.updateapputils.extension.installApk
import com.yundashi.updateapputils.extension.log
import com.yundashi.updateapputils.update.DownloadAppUtils
import com.yundashi.updateapputils.update.UpdateAppUtils
import com.yundashi.updateapputils.update.UpdateAppUtils.getInstance
import com.yundashi.updateapputils.util.AppUtil
import com.yundashi.updateapputils.util.YXServiceManager
import com.zcapi

class ApkBroadCastReceiver : BroadcastReceiver() {
    private val updateConfig by lazy { UpdateAppUtils.updateInfo.config }
//    private val handler=Handler()
//    private val runnable = Runnable{
//        val checkPackInfo =
//            AppUtil.instance.checkPackInfo(updateConfig.packageName, context)
//        if (checkPackInfo) {
//            log("ApkBroadCastReceiver-installApk-return")
//            return
//        } else {
//            log("ApkBroadCastReceiver-installApk-again")
//            installApk(context)
//        }
//    }


    override fun onReceive(context: Context, intent: Intent) {
        log("onReceive:${intent.action}")

        if (Intent.ACTION_PACKAGE_ADDED == intent.action) {
            getInstance().deleteInstalledApk()
            UpdateAppActivity.activity?.finish()
        }
        if (Intent.ACTION_PACKAGE_REMOVED == intent.action) {
            installApk(context)
        }

        if (Intent.ACTION_PACKAGE_REPLACED == intent.action) {
            getInstance().deleteInstalledApk()
            UpdateAppActivity.activity?.finish()
        }
    }

    /**
     * 静默安装
     * */
    private fun installApk(context: Context) {
        //安装模式（0默认安装 2俊鹏广播安装 3印象service安装）
        log("ApkBroadCastReceiver-installApk-${updateConfig.installType}")
        when (updateConfig.installType) {
            0 -> {
                // 默认安装apk
                context.installApk(DownloadAppUtils.downloadUpdateApkFilePath)
            }
            1 -> {
                //静默安装
                AppUtil.instance.rootInstallApk(DownloadAppUtils.downloadUpdateApkFilePath)
            }
            2 -> {
                val intent = Intent("com.mobilepower.terminal.message")
                intent.putExtra("type", 3)
                intent.putExtra("path", DownloadAppUtils.downloadUpdateApkFilePath)
                log("ApkBroadCastReceiver-path===${DownloadAppUtils.downloadUpdateApkFilePath}")
                context.sendBroadcast(intent)
            }
            3 -> {
                YXServiceManager.inStallApk(context, DownloadAppUtils.downloadUpdateApkFilePath)
            }
            7 -> {
                var zcapi = zcapi()
                zcapi.getContext(context)
                zcapi.InstallApk(DownloadAppUtils.downloadUpdateApkFilePath,true)
            }
            else -> {
                // 默认安装apk
                context.installApk(DownloadAppUtils.downloadUpdateApkFilePath)
            }
        }
    }
}