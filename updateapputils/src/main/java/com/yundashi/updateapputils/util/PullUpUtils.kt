package com.yundashi.updateapputils.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.text.TextUtils


object PullUpUtils {

    /**
     * 根据URL拉起App
     *
     *
     * 优点：不暴露包命
     * 缺点：host path schemeA应用和B应用得规定死
     *
     * @param activity 调用者Activity
     * @param uriString  Uri地址 如 "csd://com.xuanyuan.pullup/cyn?type=110"
     * 注意：1.manifest文件对应Activity 添加 android:exported="true"
     *      2.不添加exported="true"则无法找到该Activity
     *      3.不一定是启动Activity
     *
     * 需要拉起的Activity,在manifest文件<activity></activity>间需要配置如下
     * <intent-filter>
     *  <data
     *      android:host="pull.csd.demo"
     *      android:path="/cyn"
     *      android:scheme="csd" />
     *
     *      <action android:name="android.intent.action.VIEW" />
     *      <category android:name="android.intent.category.DEFAULT" />
     *      <category android:name="android.intent.category.BROWSABLE" />
     * </intent-filter>
     */
    fun pullUpByUri(activity: Activity, uriString: String) {
        val intent = Intent()
        intent.data = Uri.parse(uriString)
        //这里Intent当然也可传递参数,但是一般情况下都会放到上面的URL中进行传递
        intent.putExtra("type", "100")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        activity.startActivity(intent)
    }

    /**
     * 根据包名、Activit来拉起App
     * 此方式拉起的App在原程序运行，不会再单独开启一个app应用
     *
     * @param activity 调用者Activity
     * @param pkgName      包名
     * @param acName   要拉起的Activity名,
     * 注意：1.manifest文件对应Activity 添加 android:exported="true"
     *      2.不添加exported="true"则无法找到该Activity
     *      3.不一定是启动Activity
     */
    fun pullUpByPackage(activity: Activity, pkgName: String, acName: String) {
        val intent = Intent(Intent.ACTION_MAIN)
        // 知道要跳转应用的包命与目标Activity
        val componentName = ComponentName(pkgName, acName)
        intent.component = componentName
        // 这里Intent传值
        intent.putExtra("key", "value")
        activity.startActivity(intent)
    }

    /**
     * 仅仅通过包名进行调用,拉起的是启动页
     * @param activity 调用者Activity
     * @param pkgName  包名
     * 注意：1.manifest文件对应Activity 添加 android:exported="true"
     *      2.不添加exported="true"则无法找到该Activity
     *      3.不一定是启动Activity
     */
    fun pullUpOnlyPackage(activity: Activity, pkgName: String) {
        val intent: Intent? = activity.packageManager.getLaunchIntentForPackage(pkgName)
        if (intent != null) {
            intent.putExtra("type", "110")
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            activity.startActivity(intent)
        }
    }

    /**
     * 获取指定包名的activity 的 Intent
     */
    @SuppressLint("WrongConstant", "QueryPermissionsNeeded")
    fun getAppOpenIntentByPackageName(context: Context, packageName: String): Intent? {
        //Activity完整名
        var mainAct: String? = null
        //根据包名寻找
        val pkgMag = context.packageManager
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        intent.flags = Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_NEW_TASK
        val list = pkgMag.queryIntentActivities(intent, PackageManager.GET_ACTIVITIES)
        for (i in list.indices) {
            val info = list[i]
            if (info.activityInfo.packageName == packageName) {
                mainAct = info.activityInfo.name
                break
            }
        }
        if (TextUtils.isEmpty(mainAct)) {
            return null
        }
        intent.component = ComponentName(packageName, mainAct!!)
        return intent
    }

}