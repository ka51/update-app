package com.yundashi.updateapputils.util;

import android.content.Context;

import com.proembed.service.MyService;

/**
 * @description:
 * @author: Liu Liangze
 * @date : 2021/7/20 17:27
 */
public class YXServiceManager {
    private static MyService myService;

    //安装
    public static void inStallApk(Context context, String apkPath) {
        getMyService(context).silentInstallApk(apkPath, context.getPackageName(), true);
    }

    //卸载
    public static void unInStallApk(Context context, String packageName) {
        getMyService(context).silentUnInstallApk(packageName);
    }

    private static MyService getMyService(Context context) {
        if (myService == null) {
            myService = new MyService(context);
        }
        return myService;
    }
}
